
public class Room {
	
	private String wallcolor;
	private int window;
	private String floor;
	/**
	 * this is the constructor that takes no input arguments
	 */
	public Room() {
		this.wallcolor = "not defined";
		this.window = 0;
		this.floor = "not defined";
	}
	/**
	 * This is the overloaded constructor, you must enter wall color first, then number of windows, then floor type.
	 * @param wallcolor
	 * @param window
	 * @param floor
	 */
	public Room(String wallcolor, int window, String floor) {
		this.wallcolor = wallcolor;
		this.window = window;
		this.floor = floor;
	}
	/**
	 * this is used to return just the wall color from the room
	 * @return
	 */
	public String getwallcolor(){
		return wallcolor;
	}
	/**
	 * this is used to return just the number of windows from the room
	 * @return
	 */
	public int getwindow(){
		return window;
	}
	/**
	 * this is used to return just the floor type from the room
	 * @return
	 */
	public String getfloor(){
		return floor;
	}
	/**
	 * this allows us to change the wall color of an existing room
	 * @param wallcolor
	 */
	public void setwallcolor(String wallcolor){
		this.wallcolor = wallcolor;
	}
	/**
	 * this allows us to change the number of windows in an existing room
	 * @param window
	 */
	public void setwindow(int window){
		this.window = window;
	}
	/**
	 * this allows us to change the floor type of an existing room
	 * @param floor
	 */
	public void setfloor(String floor){
		this.floor = floor;
	}
	/**
	 * this is the print method, it will return all the specifics of the room
	 * @return
	 */
	public String getRoom(){
		return wallcolor + " " + window + " " + floor;
	}
	
	
	}



public class Maintest {

	public static void main(String[] args){
		
		//testing the no argument constructor
		Room roomnoarguments = new Room();
		System.out.println(roomnoarguments.getRoom());
		
		//testing the overloaded constructor
		Room roomoverloaded = new Room("yellow", 1, "carpet" );
		System.out.println(roomoverloaded.getRoom());
		
		
		//testing the get methods 
		System.out.println(roomoverloaded.getwallcolor());
		System.out.println(roomoverloaded.getwindow());
		System.out.println(roomoverloaded.getfloor());
		
		
		//testing the set methods
		roomoverloaded.setwallcolor("blue");
		System.out.println(roomoverloaded.getwallcolor());
		
		roomoverloaded.setwindow(3);
		System.out.println(roomoverloaded.getwindow());
		
		roomoverloaded.setfloor("tile");
		System.out.println(roomoverloaded.getfloor());
		
		
		// creating the 3 rooms specified in the rubric
		Room room1 = new Room("yellow", 1, "hard wood");
		System.out.println(room1.getRoom());
		
		Room room2 = new Room("purple", 0, "tiled");
		System.out.println(room2.getRoom());
		
		Room room3 = new Room("white", 3, "carpeted");
		System.out.println(room3.getRoom());
		
	}
}

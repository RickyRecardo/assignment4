
public class Room {

	private String wallcolor;
	private int window;
	private String floor;
	
	public Room() {
		this.wallcolor = "not defined";
		this.window = 0;
		this.floor = "not defined";
	}
	
	public Room(String wallcolor, int window, String floor) {
		this.wallcolor = wallcolor;
		this.window = window;
		this.floor = floor;
	}
	
	public String getwallcolor(){
		return wallcolor;
	}
	
	public int getwindow(){
		return window;
	}
	
	public String getfloor(){
		return floor;
	}
	
	public void setwallcolor(String wallcolor){
		this.wallcolor = wallcolor;
	}
	
	public void setwindow(int window){
		this.window = window;
	}
	
	public void setfloor(String floor){
		this.floor = floor;
	}
	
	public String getRoom(){
		return wallcolor + " " + window + " " + floor;
	}
	
	
	}

